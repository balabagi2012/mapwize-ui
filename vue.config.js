module.exports = {
  publicPath: "/mapwize",
  productionSourceMap:false,
  assetsDir:'static',
  devServer: {
    disableHostCheck: true,
  },
};
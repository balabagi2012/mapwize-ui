export default class Api {
  // static API_BASE = "http://localhost:3000/";
  static API_BASE = "http://test.batalk.fun:3000/";
  static async getDirection(from, to) {
    try {
      const response = await fetch(`${this.API_BASE}`, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          from,
          to
        })
      });
      const responseJson = await response.json();
      return responseJson;
    } catch (error) {
      console.log(error);
    }
  }
}
